import 'dart:isolate';

main() {
  start();

  // 执行耗时的任务
  newIsolate();

  init();
}

start() {
  print('应用启动：'+DateTime.now().microsecondsSinceEpoch.toString());
}

newIsolate() async {
  print('新线程创建');
  ReceivePort r = ReceivePort();
  SendPort p = r.sendPort;
  // 创建新线程
  Isolate childIsolate = await Isolate.spawnUri(
    Uri(path: "childIsolate.dart"), 
    ['data1', 'data2', 'data3'], 
    p
  );

  // 监听消息
  r.listen((message) {
    print('主线程接收到数据：$message[0]');
    if (message[1] == 1) {
      // 异步任务正在处理
    } else if (message[1] == 2) {
      r.close(); // 取消监听
      childIsolate.kill(); // 杀死新线程，释放资源
      print('子线程已经释放');
    }
  });
}

init() {
  print('项目初始化');
}