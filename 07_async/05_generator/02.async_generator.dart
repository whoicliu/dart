main() {
  final Stream<int> s = asycnCountDown(5);

  print('start');
  s.listen((event) {print(event);})
    .onDone(() {print('Done');});
  print('end');
}

// 异步生成器
Stream<int> asycnCountDown(int n) async* {
  while (n > 0) {
    yield n--;
  }
}