main() {
  var res = getNumber(5).iterator;

  while (res.moveNext()) {
    print(res.current);
  }
  // res.moveNext(); print(res.current);
  // res.moveNext(); print(res.current);
  // res.moveNext(); print(res.current);
  // res.moveNext(); print(res.current);
  // res.moveNext(); print(res.current);
  // res.moveNext(); print(res.current);
  // res.moveNext(); print(res.current);
}

// 同步生成器
Iterable<int> getNumber(int n) sync* {
  print('start');
  int i = 0;
  while (i < n) {
    yield i++;
  }
  print('end');
}