main() {
  final Iterable<int> s = getRange(1, 6);
  print('start');
  s.forEach((element) {print(element);});
  print('end');
}

// 同步递归生成器
Iterable<int> getRange(int start, int end) sync* {
  if (start <= end) {
    yield start;
    // 实现递归调用
    // for (final val in getRange(start+1, end)) {
    //   yield val;
    // }

    yield* getRange(start+1, end);
  }
}