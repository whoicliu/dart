main() {
  var i = 0;
  Future.doWhile(() {
    i++;
    return Future.delayed(Duration(seconds: 2), () {
      print('Future.doWhile() $i, 当前时间：'+DateTime.now().microsecondsSinceEpoch.toString());
      return i < 6;
    }).then((value) {
      print(value);
      return value;
    });
  }).then((value) {
    print('Future.doWhile() then: '+value.toString());
  });
}