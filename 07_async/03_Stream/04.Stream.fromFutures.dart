Future<String> getData() {
  return Future.delayed(Duration(seconds: 2), () {
    return '当前时间：${DateTime.now()}';
  });
}

main() {
  var data = [getData(), getData(), getData(), getData()];
  Stream.fromFutures(data)
    .listen((event) {print('Stream.fromFutures : $event');})
    .onDone(() {print('Stream.fromFutures done');});
}