main() {
  Duration interval = Duration(seconds: 1);

  // 如果不设置第二个参数，则默认返回 null
  // Stream<int> stream = Stream<int>.periodic(interval);
  // stream.listen((event) {
  //   print('Stream.periodic: $event');
  // }).onDone(() {
  //   print('Stream.periodic done');
  // });

  // 带有第二个参数，则返回具体的数据
  Stream<int> streamData = Stream<int>.periodic(interval, (data) => data);
  streamData.take(5).listen((event) {
    print('Stream.periodic: $event');
  }).onDone(() {
    print('Stream.periodic done');
  });
}