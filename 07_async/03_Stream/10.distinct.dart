main() {
  var data = [1, 2, 'a', 'a', true, true, false, true, 'a'];
  Stream.fromIterable(data)
    .distinct() // 去掉与前一个相同的数据(连续重复的值)
    .listen((event) => print('Stream.fromIterable -> $event'))
    .onDone(() => print('Stream.fromIterable -> done 结束'));
}