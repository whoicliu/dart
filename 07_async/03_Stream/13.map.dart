main() {
  Duration interval = Duration(seconds: 1);

  Stream<int> streamData = Stream<int>.periodic(interval, (data) => data + 1);

  streamData.takeWhile((element) {
    // print('takeWhile -> $element');
    return element <= 3;
  }).map((event) {
    print('map -> $event -> ${event * 100}');
    return event * 100;
  }).listen((event) {
    print('listen-> $event');
  }).onDone(() {
    print('Done 结束');
  });
}