class Person {
  say() {
    print('Say something');
  }
}

class PersonSingleton {
  static PersonSingleton instance;

  PersonSingleton._internal();

  static getInstance() {
    if (instance == null) {
      instance = PersonSingleton._internal();
    }
  }

  factory PersonSingleton() => getInstance();
}

main() {
  var p1 = Person();
  print(p1.hashCode);

  var p2 = Person();
  print(p2.hashCode);
  print(p1 == p2);

  var ps1 = PersonSingleton();
  print(ps1.hashCode);
  var ps2 = PersonSingleton();
  print(ps2.hashCode);
  print(ps1 == ps2);
}